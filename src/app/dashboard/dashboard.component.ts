import { Component, OnInit } from '@angular/core';
import { Hero, HeroImpl } from '../HeroTools/hero';
import { HeroService } from '../HeroTools/hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    console.log("dashbord getheroes");
    this.heroService.getBestTeam()
      .subscribe(backBestTeam => {
        this.heroes = backBestTeam.map(item => new HeroImpl(item.name, item.id)); 
      });
  }
}