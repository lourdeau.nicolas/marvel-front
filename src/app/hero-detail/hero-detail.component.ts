import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../HeroTools/hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HeroService } from '../HeroTools/hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  @Input() hero?: Hero;
  favorite: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getHero();
  }
  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => 
        {
          this.hero = hero;
          this.heroService.isHeroFavorite(this.hero).subscribe(bool => this.favorite = bool);
        });
  }
  goBack(): void {
    this.location.back();
  }

  addToBestTeam(): void {
    if (this.hero != undefined){
    this.heroService.addHeroToBestTeam(this.hero);
    this.favorite = true;
    }
  }
  removeFromBestTeam(): void {
    if (this.hero != undefined){
    this.heroService.removeHeroFromBestTeam(this.hero);
    this.favorite = false;
    }  
  }
}
