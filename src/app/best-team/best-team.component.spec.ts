import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BestTeamComponent } from './best-team.component';

describe('BestTeamComponent', () => {
  let component: BestTeamComponent;
  let fixture: ComponentFixture<BestTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BestTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BestTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
