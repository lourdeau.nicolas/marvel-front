import { Component, OnInit } from '@angular/core';
import { Hero } from '../HeroTools/hero';
import { HeroService } from '../HeroTools/hero.service';

@Component({
  selector: 'app-best-team',
  templateUrl: './best-team.component.html',
  styleUrls: ['./best-team.component.css']
})
export class BestTeamComponent implements OnInit {

  heroes: Hero[] = [];
  constructor(
    private heroService: HeroService
  ) { }

  ngOnInit(): void {
  }

  getHero(): void {
    
    this.heroService.getBestTeam()
      .subscribe(heroes => this.heroes = heroes);
  }

}
