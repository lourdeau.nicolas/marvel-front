import { Injectable } from '@angular/core';
import { Hero, HeroImpl } from './hero';
import { HEROES } from './mockHeroes';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Md5 } from 'ts-md5';
import { marvelCharactersResponse } from './marvelCharactersResponse';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { BestTeamComponent } from '../best-team/best-team.component';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private heroesUrl = 'https://gateway.marvel.com:443/v1/public/characters?apikey=b43180526e06f2a9ba98a9122d89ad43'; 
  private backUrl = "http://localhost:9090";
  private md5 = new Md5;
  private ts = new Date().getTime().toString();
  jsonResponse: any; 
  parsedResponse: any;
  constructor(
    private http: HttpClient
  ) { }

  getHeroes(): Observable<Hero[]> {
    // TS
    this.md5.appendStr(this.ts);
    // PPK
    this.md5.appendStr('MyPPK');
    // PUBK
    this.md5.appendStr('b43180526e06f2a9ba98a9122d89ad43');

    this.http.get(this.heroesUrl.concat("&ts=", this.ts, "&hash=", this.md5.end().toString())).subscribe(marvelResponse => {
      this.jsonResponse = JSON.stringify(marvelResponse)
      //console.log("ma jsonResponse" + this.jsonResponse);
      this.parsedResponse = JSON.parse(this.jsonResponse)
      //console.log("ma parsedResponse" + this.parsedResponse);

    });
    const heroes = of(HEROES);
    return heroes;
  }




  getBestTeam(): Observable<Hero[]>  {
    var myheroes: Observable<Hero[]> = 
    this.http.get<Hero[]>(this.backUrl.concat("/team/all?token=n8NztO0hInqU_39Q_HAAmrXqsl90QoYt"))
    console.log("team" + JSON.stringify(myheroes));
    return myheroes;
  }

  addHeroToBestTeam(hero: Hero): void {
    this.http.post(this.backUrl.concat("/team/add?token=n8NztO0hInqU_39Q_HAAmrXqsl90QoYt&name=", hero.name, "&id=", hero.id.toString()),null).subscribe(backBestTeam => {
      
      console.log("ma réponse suite ajout " + backBestTeam);

    });
  }

  getHero(id: number): Observable<Hero> {
    const hero = HEROES.find(h => h.id === id)!;
    return of(hero);
  }

  isHeroFavorite(hero: Hero): Observable<boolean> {
    return this.http.get<boolean>(this.backUrl.concat("/hero/fav?token=n8NztO0hInqU_39Q_HAAmrXqsl90QoYt&id=",hero.id.toString()));
  }
  removeHeroFromBestTeam(hero: Hero): void {
    this.http.post(this.backUrl.concat("/team/del?token=n8NztO0hInqU_39Q_HAAmrXqsl90QoYt&id=", hero.id.toString()),null).subscribe(backBestTeam => {
      
      console.log("ma réponse suite del " + backBestTeam);

    });
  }
}
