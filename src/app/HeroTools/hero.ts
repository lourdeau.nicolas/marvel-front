export interface Hero {
    id: number;
    name: string;
  }

export class HeroImpl implements Hero {
  id: number;
  name: string;

  constructor(name: string, id: number){
    this.id = id;
    this.name = name;
  };
  
}
