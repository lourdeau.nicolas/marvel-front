export interface marvelCharactersResponse {
    code: number;
    status: string; //"status": "Ok",
    copyright: string //"© 2021 MARVEL",
    attributionText: string // "Data provided by Marvel. © 2021 MARVEL",
    attributionHTML: string // "<a href=\"http://marvel.com\">Data provided by Marvel. © 2021 MARVEL</a>",
    etag: string // "710bf39a70ef97fa4e6cdd7e84b701dae0ef63a4",
    data: any;
}
